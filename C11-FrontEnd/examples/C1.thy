(******************************************************************************
 * Isabelle/C
 *
 * Copyright (c) 2018-2019 Université Paris-Saclay, Univ. Paris-Sud, France
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of the copyright holders nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************)

chapter \<open>Example: Annotation Navigation and Context Serialization\<close>

theory C1
  imports Isabelle_C.C_Main
          "HOL-ex.Cartouche_Examples"
begin

text \<open> Operationally, the \<^theory_text>\<open>C\<close> command can be thought of as
behaving as \<^theory_text>\<open>ML\<close>, where it is for example possible to recursively nest C
code in C. Generally, the present chapter assumes a familiarity with all advance concepts of ML as
described in \<^file>\<open>~~/src/HOL/ex/ML.thy\<close>, as well as the concept of ML
antiquotations (\<^file>\<open>~~/src/Doc/Implementation/ML.thy\<close>). However, even if
\<^theory_text>\<open>C\<close> might resemble to \<^theory_text>\<open>ML\<close>, we will now see
in detail that there are actually subtle differences between the two commands.\<close>

section \<open>Setup of ML Antiquotations Displaying the Environment (For Debugging) \<close>

ML\<open>
fun print_top make_string f _ (_, (value, _, _)) _ = tap (fn _ => writeln (make_string value)) o f

fun print_top' _ f _ _ env = tap (fn _ => writeln ("ENV " ^ C_Env.string_of env)) o f

fun print_stack s make_string stack _ _ thy =
  let
    val () = Output.information ("SHIFT  " ^ (case s of NONE => "" | SOME s => "\"" ^ s ^ "\" ")
                                 ^ Int.toString (length stack - 1) ^ "    +1 ")
    val () =   stack
            |> split_list
            |> #2
            |> map_index I
            |> app (fn (i, (value, pos1, pos2)) =>
                     writeln ("   " ^ Int.toString (length stack - i) ^ " " ^ make_string value
                              ^ " " ^ Position.here pos1 ^ " " ^ Position.here pos2))
  in thy end

fun print_stack' s _ stack _ env thy =
  let
    val () = Output.information ("SHIFT  " ^ (case s of NONE => "" | SOME s => "\"" ^ s ^ "\" ")
                                 ^ Int.toString (length stack - 1) ^ "    +1 ")
    val () = writeln ("ENV " ^ C_Env.string_of env)
  in thy end
\<close>

setup \<open>ML_Antiquotation.inline @{binding print_top}
                               (Args.context
                                >> K ("print_top " ^ ML_Pretty.make_string_fn ^ " I"))\<close>
setup \<open>ML_Antiquotation.inline @{binding print_top'}
                               (Args.context
                                >> K ("print_top' " ^ ML_Pretty.make_string_fn ^ " I"))\<close>
setup \<open>ML_Antiquotation.inline @{binding print_stack}
                               (Scan.peek (fn _ => Scan.option Args.text)
                                >> (fn name => ("print_stack "
                                                ^ (case name of NONE => "NONE"
                                                              | SOME s => "(SOME \"" ^ s ^ "\")")
                                                ^ " " ^ ML_Pretty.make_string_fn)))\<close>
setup \<open>ML_Antiquotation.inline @{binding print_stack'}
                               (Scan.peek (fn _ => Scan.option Args.text)
                                >> (fn name => ("print_stack' "
                                                ^ (case name of NONE => "NONE"
                                                              | SOME s => "(SOME \"" ^ s ^ "\")")
                                                ^ " " ^ ML_Pretty.make_string_fn)))\<close>

declare[[C_lexer_trace]]

section \<open>Introduction to C Annotations: Navigating in the Parsing Stack\<close>

subsection \<open>Basics\<close>

text \<open> Since the present theory \<^file>\<open>C1.thy\<close> is depending on
\<^theory>\<open>Isabelle_C.C_Lexer_Language\<close> and
\<^theory>\<open>Isabelle_C.C_Parser_Language\<close>, the syntax one is writing in the
\<^theory_text>\<open>C\<close> command is C11. Additionally, \<^file>\<open>C1.thy\<close> also
depends on \<^theory>\<open>Isabelle_C.C_Parser_Annotation\<close>, making it possible to write
commands in C comments, called annotation commands, such as
\<^theory_text>\<open>\<approx>setup\<close>. \<close>

C \<comment> \<open>Nesting ML code in C comments\<close> \<open>
int a = (((0))); /*@ highlight */
                 /*@ \<approx>setup \<open>@{print_stack}\<close> */
                 /*@ \<approx>setup \<open>@{print_top}\<close> */
\<close>

text \<open> In terms of execution order, nested annotation commands are not pre-filtered out of the
C code, but executed when the C code is still being parsed. Since the parser implemented is a LALR
parser \<^footnote>\<open>\<^url>\<open>https://en.wikipedia.org/wiki/LALR\<close>\<close>, C tokens
are uniquely read and treated from left to right. Thus, each nested command is (supposed by default
to be) executed when the parser has already read all C tokens before the comment associated to the
nested command, so when the parser is in a particular intermediate parsing step (not necessarily
final)
\<^footnote>\<open>\<^url>\<open>https://en.wikipedia.org/wiki/Shift-reduce_parser\<close>\<close>. \<close>

text \<open>The command \<^theory_text>\<open>\<approx>setup\<close> is similar to the command
\<^theory_text>\<open>setup\<close> except that the former takes a function with additional
arguments. These arguments are precisely depending on the current parsing state. To better examine
these arguments, it is convenient to use ML antiquotations (be it for printing, or for doing any
regular ML actions like PIDE reporting).

Note that, in contrast with \<^theory_text>\<open>setup\<close>, the return type of the
\<^theory_text>\<open>\<approx>setup\<close> function is not
\<^ML_type>\<open>theory -> theory\<close> but
\<^ML_type>\<open>Context.generic -> Context.generic\<close>. \<close>

C \<comment> \<open>Positional navigation: referring to any previous parsed sub-tree in the stack\<close> \<open>
int a = (((0
      + 5)))  /*@@ \<approx>setup \<open>print_top @{make_string} I\<close>
                 @ highlight
               */
      * 4; 
float b = 7 / 3;
\<close>

text \<open>The special \<open>@\<close> symbol makes the command be executed whenever the first element \<open>E\<close>
 in the stack is about to be irremediably replaced by a more structured parent element (having \<open>E\<close>
as one of its direct children). It is the parent element which is provided to the ML code.

Instead of always referring to the first element of the stack, 
\<open>N\<close> consecutive occurrences of \<open>@\<close> will make the ML code getting as argument the direct parent
of the \<open>N\<close>-th element.\<close>

C \<comment> \<open>Positional navigation: referring to any previous parsed sub-tree in the stack\<close> \<open>
int a = (((0 + 5)))  /*@@ highlight */
      * 4;

int a = (((0 + 5)))  /*@& highlight */
      * 4;

int a = (((0 + 5)))  /*@@@@@ highlight */
      * 4;

int a = (((0 + 5)))  /*@&&&& highlight */
      * 4;
\<close>

text \<open>\<open>&\<close> behaves as \<open>@\<close>, but instead of always giving the designated direct parent to the ML code,
it finds the first parent ancestor making non-trivial changes in the respective grammar rule
(a non-trivial change can be for example the registration of the position of the current AST node
being built).\<close>

C \<comment> \<open>Positional navigation: moving the comment after a number of C token\<close> \<open>
int b = 7 / (3) * 50;
/*@+++@@ highlight */
long long f (int a) {
  while (0) { return 0; }
}
int b = 7 / (3) * 50;
\<close>

text \<open>\<open>N\<close> consecutive occurrences of \<open>+\<close> will delay the interpretation of the comment,
which is ignored at the place it is written. The comment is only really considered after the
C parser has treated \<open>N\<close> more tokens.\<close>

C \<comment> \<open>Closing C comments \<open>*/\<close> must close anything, even when editing ML code\<close> \<open>
int a = (((0 //@ (* inline *) \<approx>setup \<open>fn _ => fn _ => fn _ => fn context => let in (* */ *) context end\<close>
             /*@ \<approx>setup \<open>(K o K o K) I\<close> (*   * /   *) */
         )));
\<close>

C \<comment> \<open>Inline comments with antiquotations\<close> \<open>
 /*@ \<approx>setup\<open>(K o K o K) (fn x => K x @{con\
text (**)})\<close> */ // break of line activated everywhere (also in antiquotations)
int a = 0; //\
@ \<approx>setup\<open>(K o K o K) (fn x => K x @{term \<open>a \
          + b\<close> (* (**) *\      
\     
)})\<close>
\<close>

subsection \<open>Erroneous Annotations Treated as Regular C Comments\<close>

C \<comment> \<open>Permissive Types of Antiquotations\<close> \<open>
int a = 0;
  /*@ \<approx>setup (* Errors: Explicit warning + Explicit markup reporting *)
   */
  /** \<approx>setup (* Errors: Turned into tracing report information *)
   */

  /** \<approx>setup \<open>fn _ => fn _ => fn _ => I\<close> (* An example of correct syntax accepted as usual *)
   */
\<close>

C \<comment> \<open>Permissive Types of Antiquotations\<close> \<open>
int a = 0;
  /*@ \<approx>setup \<open>fn _ => fn _ => fn _ => I\<close>
      \<approx>setup (* Parsing error of a single command does not propagate to other commands *)
      \<approx>setup \<open>fn _ => fn _ => fn _ => I\<close>
      context
   */
  /** \<approx>setup \<open>fn _ => fn _ => fn _ => I\<close>
      \<approx>setup (* Parsing error of a single command does not propagate to other commands *)
      \<approx>setup \<open>fn _ => fn _ => fn _ => I\<close>
      context
   */
  
  /*@ \<approx>setup (* Errors in all commands are all rendered *)
      \<approx>setup (* Errors in all commands are all rendered *)
      \<approx>setup (* Errors in all commands are all rendered *)
   */
  /** \<approx>setup (* Errors in all commands makes the whole comment considered as an usual comment *)
      \<approx>setup (* Errors in all commands makes the whole comment considered as an usual comment *)
      \<approx>setup (* Errors in all commands makes the whole comment considered as an usual comment *)
   */
\<close>

subsection \<open>Bottom-Up vs. Top-Down Evaluation\<close>

ML\<open>
structure Example_Data = Generic_Data (type T = string list
                                       val empty = [] val extend = K empty val merge = K empty)
fun add_ex s1 s2 =
  Example_Data.map (cons s2)
  #> (fn context => let val () = Output.information (s1 ^ s2)
                        val () = app (fn s => writeln ("  Data content: " ^ s))
                                     (Example_Data.get context)
                    in context end)
\<close>

setup \<open>Context.theory_map (Example_Data.put [])\<close>

declare[[ML_source_trace]]
declare[[C_parser_trace]]

C \<comment> \<open>Arbitrary interleaving of effects: \<open>\<approx>setup\<close> vs \<open>\<approx>setup\<Down>\<close>\<close> \<open>
int b,c,d/*@@ \<approx>setup \<open>fn s => fn x => fn env => @{print_top} s x env
                                                #> add_ex "evaluation of " "3_print_top"\<close>
          */,e = 0; /*@@
              \<approx>setup \<open>fn s => fn x => fn env => @{print_top} s x env
                                                #> add_ex "evaluation of " "4_print_top"\<close> */

int b,c,d/*@@ \<approx>setup\<Down> \<open>fn s => fn x => fn env => @{print_top} s x env
                                                #> add_ex "evaluation of " "6_print_top"\<close>
          */,e = 0; /*@@
              \<approx>setup\<Down> \<open>fn s => fn x => fn env => @{print_top} s x env
                                                #> add_ex "evaluation of " "5_print_top"\<close> */
\<close>

subsection \<open>Out of Bound Evaluation for Annotations\<close>

C \<comment> \<open>Bottom-up and top-down + internal initial value\<close> \<open>
int a = 0 ;
int     /*@ @    ML \<open>writeln "2"\<close>
            @@@  ML \<open>writeln "4"\<close>
            +@   ML \<open>writeln "3"\<close>
(*            +++@ ML \<open>writeln "6"\<close>*)
                 ML\<Down>\<open>writeln "1"\<close>  */
//    a d /*@ @    ML \<open>writeln "5"\<close>  */;
int a;
\<close>

C \<comment> \<open>Ordering of consecutive commands\<close> \<open>
int a = 0  /*@ ML\<open>writeln "1"\<close> */;
int        /*@ @@@@@ML\<open>writeln "5" \<close> @@@ML\<open>writeln "4" \<close> @@ML\<open>writeln "2" \<close> */
           /*@ @@@@@ML\<open>writeln "5'"\<close> @@@ML\<open>writeln "4'"\<close> @@ML\<open>writeln "2'"\<close> */
    a = 0;
int d = 0; /*@ ML\<open>writeln "3"\<close> */
\<close>

C \<comment> \<open>Maximum depth reached\<close> \<open>
int a = 0 /*@ ++@@@@ML\<open>writeln "2"\<close>
              ++@@@ ML\<open>writeln "1"\<close> */;
\<close>

section \<open>Reporting of Positions and Contextual Update of Environment\<close>

text \<open>
To show the content of the parsing environment, the ML antiquotations \<open>print_top'\<close> and \<open>print_stack'\<close>
will respectively be used instead of \<open>print_top\<close> and \<open>print_stack\<close>. 
This example suite allows to explore the bindings represented in the C environment 
and made accessible in PIDE for hovering. \<close>

subsection \<open>Reporting: \<open>typedef\<close>, \<open>enum\<close>\<close> (*\<open>struct\<close>*)

declare [[ML_source_trace = false]]
declare [[C_lexer_trace = false]]

C \<comment> \<open>Reporting of Positions\<close> \<open>
typedef int i, j;
  /*@@ \<approx>setup \<open>@{print_top'}\<close> @highlight */ //@ +++++@ \<approx>setup \<open>@{print_top'}\<close> +++++@highlight
int j = 0;
typedef int i, j;
j jj1 = 0;
j jj = jj1;
j j = jj1 + jj;
typedef i j;
typedef i j;
typedef i j;
i jj = jj;
j j = jj;
\<close>

C \<comment> \<open>Nesting type definitions\<close> \<open>
typedef int j;
j a = 0;
typedef int k;
int main (int c) {
  j b = 0;
  typedef int k;
  typedef k l;
  k a = c;
  l a = 0;
}
k a = a;
\<close>

C \<comment> \<open>Reporting \<open>enum\<close>\<close> \<open>
enum a b; // bound case: undeclared
enum a {aaa}; // define case
enum a {aaa}; // define case: redefined
enum a _; // bound case

__thread (f ( enum a,  enum a vv));

enum a /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.function_definition4\<close>\<close>*/ f (enum a a) {
}

__thread enum a /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.declaration_specifier2\<close>\<close>*/ f (enum a a) {
  enum c {ccc}; // define case
  __thread enum c f (enum c a) {
    return 0;
  }
  enum c /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.nested_function_definition2\<close>\<close>*/ f (enum c a) {
    return 0;
  }
  return 0;
}

enum z {zz}; // define case
int main (enum z *x) /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.parameter_type_list2\<close>\<close>*/ {
  return zz; }
int main (enum a *x, ...) /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.parameter_type_list3\<close>\<close>*/ {
  return zz; }
\<close>

subsection \<open>Continuation Calculus with the C Environment: Presentation in ML\<close>

declare [[C_parser_trace = false]]

ML\<open>
val C = tap o C_Module.C
val C' = C_Module.C'
\<close>

C \<comment> \<open>Nesting C code without propagating the C environment\<close> \<open>
int a = 0;
int b = 7 / (3) * 50
  /*@@@@@ \<approx>setup \<open>fn _ => fn _ => fn _ =>
               C      \<open>int b = a + a + a + a + a + a + a
                       ;\<close> \<close> */;
\<close>

C \<comment> \<open>Nesting C code and propagating the C environment\<close> \<open>
int a = 0;
int b = 7 / (3) * 50
  /*@@@@@ \<approx>setup \<open>fn _ => fn _ => fn env =>
               C' env \<open>int b = a + a + a + a + a + a + a
                       ;\<close> \<close> */;
\<close>

subsection \<open>Continuation Calculus with the C Environment: Presentation with Outer Commands\<close>

ML\<open>
val _ = Theory.setup
          (C_Inner_Syntax.command0 
            (fn src => fn context => C' (C_Stack.Data_Lang.get' context |> #2) src context)
            C_Parse.C_source
            ("C'", \<^here>, \<^here>, \<^here>))
\<close>

C \<comment> \<open>Nesting C code without propagating the C environment\<close> \<open>
int f (int a) {
  int b = 7 / (3) * 50 /*@ C  \<open>int b = a + a + a + a + a + a + a;\<close> */;
  int c = b + a + a + a + a + a + a;
} \<close>

C \<comment> \<open>Nesting C code and propagating the C environment\<close> \<open>
int f (int a) {
  int b = 7 / (3) * 50 /*@ C' \<open>int b = a + a + a + a + a + a + a;\<close> */;
  int c = b + b + b + b + a + a + a + a + a + a;
} \<close>

C \<comment> \<open>Miscellaneous\<close> \<open>
int f (int a) {
  int b = 7 / (3) * 50 /*@ C  \<open>int b = a + a + a + a + a; //@ C' \<open>int c = b + b + b + b + a;\<close> \<close> */;
  int b = 7 / (3) * 50 /*@ C' \<open>int b = a + a + a + a + a; //@ C' \<open>int c = b + b + b + b + a;\<close> \<close> */;
  int c = b + b + b + b + a + a + a + a + a + a;
} \<close>

subsection \<open>Continuation Calculus with the C Environment: Deep-First Nesting vs Breadth-First Folding: Propagation of \<^ML_type>\<open>C_Env.env_lang\<close>\<close>

C \<comment> \<open>Propagation of report environment while manually composing at ML level (with \<open>#>\<close>)\<close>
  \<comment> \<open>In \<open>c1 #> c2\<close>, \<open>c1\<close> and \<open>c2\<close> should not interfere each other.\<close> \<open>
//@ ML \<open>fun C_env src _ _ env = C' env src\<close>
int a;
int f (int b) {
int c = 0; /*@ \<approx>setup \<open>fn _ => fn _ => fn env =>
     C' env \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C' env \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
\<close> */
int e = a + b + c + d;
}\<close>

C \<comment> \<open>Propagation of directive environment (evaluated before parsing)
      to any other annotations (evaluated at parsing time)\<close> \<open>
#undef int
#define int(a,b) int
#define int int
int a;
int f (int b) {
int c = 0; /*@ \<approx>setup \<open>fn _ => fn _ => fn env =>
     C' env \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C' env \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ \<approx>setup \<open>C_env \<open>int e = a + b + c + d;\<close>\<close>\<close>
\<close> */
#undef int
int e = a + b + c + d;
}
\<close>

subsection \<open>Continuation Calculus with the C Environment: Deep-First Nesting vs Breadth-First Folding: Propagation of \<^ML_type>\<open>C_Env.env_tree\<close>\<close>

ML\<open>
structure Data_Out = Generic_Data
  (type T = int
   val empty = 0
   val extend = K empty
   val merge = K empty)

fun show_env0 make_string f msg context =
  Output.information ("(" ^ msg ^ ") " ^ make_string (f (Data_Out.get context)))

val show_env = tap o show_env0 @{make_string} I
\<close>

setup \<open>Context.theory_map (C_Module.Data_Accept.put (fn _ => fn _ => Data_Out.map (fn x => x + 1)))\<close>

C \<comment> \<open>Propagation of Updates\<close> \<open>
typedef int i, j;
int j = 0;
typedef int i, j;
j jj1 = 0;
j jj = jj1; /*@@ \<approx>setup \<open>fn _ => fn _ => fn _ => show_env "POSITION 0"\<close> @\<approx>setup \<open>@{print_top'}\<close> */
typedef int k; /*@@ \<approx>setup \<open>fn _ => fn _ => fn env =>
                          C' env \<open>k jj = jj; //@@ \<approx>setup \<open>@{print_top'}\<close>
                                  k jj = jj + jj1;
                                  typedef k l; //@@ \<approx>setup \<open>@{print_top'}\<close>\<close>
                          #> show_env "POSITION 1"\<close> */
j j = jj1 + jj; //@@ \<approx>setup \<open>@{print_top'}\<close>
typedef i j; /*@@ \<approx>setup \<open>fn _ => fn _ => fn _ => show_env "POSITION 2"\<close> */
typedef i j;
typedef i j;
i jj = jj;
j j = jj;
\<close>

ML\<open>show_env "POSITION 3" (Context.Theory @{theory})\<close>

setup \<open>Context.theory_map (C_Module.Data_Accept.put (fn _ => fn _ => I))\<close>

subsection \<open>Reporting: Scope of Recursive Functions\<close>

declare [[C_starting_env = last]]

C \<comment> \<open>Propagation of Updates\<close> \<open>
int a = 0;
int b = a * a + 0;
int jjj = b;
int main (void main(int *x,int *y),int *jjj) {
  return a + jjj + main(); }
int main2 () {
  int main3 () { main2() + main(); }
  int main () { main2() + main(); }
  return a + jjj + main3() + main(); }
\<close>

C \<open>
int main3 () { main2 (); }
\<close>

declare [[C_starting_env = empty]]

subsection \<open>Reporting: Extensions to Function Types, Array Types\<close>

C \<open>int f (int z);\<close>
C \<open>int * f (int z);\<close>
C \<open>int (* f) (int z /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.declarator1\<close>\<close>*/);\<close>
C \<open>typedef int (* f) (int z);\<close>
C \<open>int f (int z) {}\<close>
C \<open>int * f (int z) {return z;}\<close>
C \<open>int ((* f) (int z1, int z2)) {return z1 + z2;}\<close>
C \<open>int (* (* f) (int z1, int z2)) {return z1 + z2;}\<close>
C \<open>typedef int (* f) (int z); f uuu (int b) {return b;};\<close>
C \<open>typedef int (* (* f) (int z, int z)) (int a); f uuu (int b) {return b;};\<close>
C \<open>struct z { int (* f) (int z); int (* (* ff) (int z)) (int a); };\<close>
C \<open>double (* (* f (int a /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Wrap_Overloading.declarator1\<close>\<close>*/)) (int a, double d)) (char a);\<close>
C \<open>double (* (((* f) []) (int a)) (int b, double c)) (char d) {int a = b + c + d;}\<close>
C \<open>double ((*((f) (int a))) (int a /* \<leftarrow>\<comment> \<open>\<^ML>\<open>C_Grammar_Rule_Lib.doFuncParamDeclIdent\<close>\<close>*/, double)) (char c) {int a = 0;}\<close>

C \<comment> \<open>Nesting functions\<close> \<open>
double (* (* f (int a)) (int a, double)) (char c) {
double (* (* f (int a)) (double a, int a)) (char) {
  return a;
}
}
\<close>

C \<comment> \<open>Old function syntax\<close> \<open>
f (x) int x; {return x;}
\<close>

section \<open>General Isar Commands\<close>

locale zz begin definition "z' = ()"
          end

C \<comment> \<open>Mixing arbitrary commands\<close> \<open>
int a = 0;
int b = a * a + 0;
int jjj = b;
/*@
  @@@ ML \<open>@{lemma \<open>A \<and> B \<longrightarrow> B \<and> A\<close> by (ml_tactic \<open>blast_tac ctxt 1\<close>)}\<close>
  definition "a' = ()"
  declare [[ML_source_trace]]
  lemma (in zz) \<open>A \<and> B \<longrightarrow> B \<and> A\<close> by (ml_tactic \<open>blast_tac ctxt 1\<close>)
  definition (in zz) "z = ()"
  corollary "zz.z' = ()"
   apply (unfold zz.z'_def)
  by blast
  theorem "True &&& True" by (auto, presburger?)
*/
\<close>

declare [[ML_source_trace = false]]

C \<comment> \<open>Backslash newlines must be supported by \<^ML>\<open>C_Token.syntax'\<close> (in particular in keywords)\<close> \<open>
//@  lem\
ma (i\
n z\
z) \
\<open>\  
AA \<and> B\
                    \<longrightarrow>\     
                    B \<and> A\    
\
A\<close> b\
y (ml_t\
actic \<open>\
bla\
st_tac c\
txt\
 0\  
001\<close>)
\<close>

section \<open>Starting Parsing Rule\<close>

subsection \<open>Basics\<close>

C \<comment> \<open>Parameterizing starting rule\<close> \<open>
/*@
declare [[C_starting_rule = "statement"]]
C \<open>while (a) {}\<close>
C \<open>a = 2;\<close>
declare [[C_starting_rule = "expression"]]
C \<open>2 + 3\<close>
C \<open>a = 2\<close>
C \<open>a[1]\<close>
C \<open>&a\<close>
C \<open>a\<close>
*/
\<close>

subsection \<open>Embedding in Inner Terms\<close>

term \<open>\<^C> \<comment> \<open>default behavior of parsing depending on the activated option\<close> \<open>0\<close>\<close>
term \<open>\<^C>\<^sub>u\<^sub>n\<^sub>i\<^sub>t \<comment> \<open>force the explicit parsing\<close> \<open>f () {while (a) {}; return 0;} int a = 0;\<close>\<close>
term \<open>\<^C>\<^sub>d\<^sub>e\<^sub>c\<^sub>l \<comment> \<open>force the explicit parsing\<close> \<open>int a = 0; \<close>\<close>
term \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r \<comment> \<open>force the explicit parsing\<close> \<open>a\<close>\<close>
term \<open>\<^C>\<^sub>s\<^sub>t\<^sub>m\<^sub>t \<comment> \<open>force the explicit parsing\<close> \<open>while (a) {}\<close>\<close>

declare [[C_starting_rule = "translation_unit"]]

term \<open>\<^C> \<comment> \<open>default behavior of parsing depending on the current option\<close> \<open>int a = 0;\<close>\<close>

subsection \<open>User Defined Setup of Syntax\<close>

setup \<open>C_Module.C_Term.map_expression (fn _ => fn _ => fn _ => @{term "10 :: nat"})\<close>
setup \<open>C_Module.C_Term.map_statement (fn _ => fn _ => fn _ => @{term "20 :: nat"})\<close>
value \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>1\<close> + \<^C>\<^sub>s\<^sub>t\<^sub>m\<^sub>t\<open>for (;;);\<close>\<close>

setup \<comment> \<open>redefinition\<close> \<open>C_Module.C_Term.map_expression
                           (fn _ => fn _ => fn _ => @{term "1000 :: nat"})\<close>
value \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>1\<close> + \<^C>\<^sub>s\<^sub>t\<^sub>m\<^sub>t\<open>for (;;);\<close>\<close>

setup \<open>C_Module.C_Term.map_default (fn _ => fn _ => fn _ => @{term "True"})\<close>

subsection \<open>Validity of Context for Annotations\<close>

ML \<open>fun fac x = if x = 0 then 1 else x * fac (x - 1)\<close>

ML \<comment> \<open>Execution of annotations in term possible in (the outermost) \<^theory_text>\<open>ML\<close>\<close> \<open>
\<^term>\<open> \<^C> \<open>int c = 0; /*@ ML \<open>fac 100\<close> */\<close> \<close>
\<close>

definition \<comment> \<open>Execution of annotations in term possible in \<^ML_type>\<open>local_theory\<close> commands (such as \<^theory_text>\<open>definition\<close>)\<close> \<open>
term = \<^C> \<open>int c = 0; /*@ ML \<open>fac 100\<close> */\<close>
\<close>

section \<open>Scopes of Inner and Outer Terms\<close>

ML \<open>
local
fun bind scan ((stack1, (to_delay, stack2)), _) =
  C_Parse.range scan
  >> (fn (src, range) =>
      C_Env.Parsing
        ( (stack1, stack2)
        , ( range
          , C_Inner_Syntax.bottom_up
              (fn _ => fn context =>
                ML_Context.exec
                  (tap (fn _ => Syntax.read_term (Context.proof_of context)
                                                 (Token.inner_syntax_of src)))
                  context)
          , Symtab.empty
          , to_delay)))
in
val _ =
  Theory.setup
    (   C_Annotation.command'
          ("term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r", \<^here>)
          ""
          (bind (C_Token.syntax' (Parse.token Parse.cartouche)))
     #> C_Inner_Syntax.command0
          (C_Inner_Toplevel.keep'' o C_Inner_Isar_Cmd.print_term)
          (C_Token.syntax' (Scan.succeed [] -- Parse.term))
          ("term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r", \<^here>, \<^here>, \<^here>))
end
\<close>

C \<open>
int z = z;
 /*@ C  \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>
     C  \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close> */\<close>
term(*outer*) \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>

C \<open>
int z = z;
 /*@ C  \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>
     C  \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close> */\<close>
term(*outer*) \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>

declare [[C_starting_env = last]]

C \<open>
int z = z;
 /*@ C  \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>
     C  \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
     C' \<open>//@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>\<close>
             term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close> */\<close>
term(*outer*) \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>z\<close>\<close>

declare [[C_starting_env = empty]]

C \<comment> \<open>Propagation of report environment while manually composing at ML level\<close> \<open>
int a;
int f (int b) {
int c = 0;
/*@ \<approx>setup \<open>fn _ => fn _ => fn env =>
     C' env \<open>int d = a + b + c + d; //@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close> term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close> term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close>\<close>
  #> C' env \<open>int d = a + b + c + d; //@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close> term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close>\<close>
  #> C      \<open>int d = a + b + c + d; //@ term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close> term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close>\<close>
\<close>
    term\<^sub>i\<^sub>n\<^sub>n\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close>
    term\<^sub>o\<^sub>u\<^sub>t\<^sub>e\<^sub>r \<open>\<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>c\<close> + \<^C>\<^sub>e\<^sub>x\<^sub>p\<^sub>r\<open>d\<close>\<close> */
int e = a + b + c + d;
}\<close>

section \<open>Calculation in Directives\<close>

subsection \<open>Annotation Command Classification\<close>

C \<comment> \<open>Lexing category vs. parsing category\<close> \<open>
int a = 0;

// \<comment> \<open>Category 2: only parsing\<close>

//@   \<approx>setup  \<open>K (K (K I))\<close> (* evaluation at parsing *)
//@@  \<approx>setup\<Down> \<open>K (K (K I))\<close> (* evaluation at parsing *)

//@   highlight             (* evaluation at parsing *)
//@@  highlight\<Down>            (* evaluation at parsing *)

// \<comment> \<open>Category 3: with lexing\<close>

//@  #setup  I              (* evaluation at lexing (and directives resolving) *)
//@   setup  I              (* evaluation at parsing *)
//@@  setup\<Down> I              (* evaluation at parsing *)

//@  #ML     I              (* evaluation at lexing (and directives resolving) *)
//@   ML     I              (* evaluation at parsing *)
//@@  ML\<Down>    I              (* evaluation at parsing *)

//@  #C     \<open>\<close>              (* evaluation at lexing (and directives resolving) *)
//@   C     \<open>\<close>              (* evaluation at parsing *)
//@@  C\<Down>    \<open>\<close>              (* evaluation at parsing *)
\<close>

C \<comment> \<open>Scheduling example\<close> \<open>
//@+++++   ML  \<open>writeln "2"\<close>
int a = 0;
//@@       ML\<Down> \<open>writeln "3"\<close>
//@       #ML  \<open>writeln "1"\<close>
\<close>

C \<comment> \<open>Scheduling example\<close> \<open>
//*  lemma True  by simp
//* #lemma True #by simp
//* #lemma True  by simp
//*  lemma True #by simp
\<close>

C \<comment> \<open>Scheduling example\<close> \<open> /*@
lemma \<open>1 = one\<close>
      \<open>2 = two\<close>
      \<open>two + one = three\<close>
by auto

#definition [simp]: \<open>three = 3\<close>
#definition [simp]: \<open>two   = 2\<close>
#definition [simp]: \<open>one   = 1\<close>
*/ \<close>

subsection \<open>Generalizing ML Antiquotations with C Directives\<close>

ML \<open>
structure Directive_setup_define = Generic_Data
  (type T = int
   val empty = 0
   val extend = K empty
   val merge = K empty)

fun setup_define1 pos f =
  C_Directive.setup_define
    pos
    (fn toks => fn (name, (pos1, _)) =>
      tap (fn _ => writeln ("Executing " ^ name ^ Position.here pos1 ^ " (only once)"))
      #> pair (f toks))
    (K I)

fun setup_define2 pos = C_Directive.setup_define pos (K o pair)
\<close>

C \<comment> \<open>General scheme of C antiquotations\<close> \<open>
/*@
    #setup \<comment> \<open>Overloading \<open>#define\<close>\<close> \<open>
      setup_define2
        \<^here>
        (fn (name, (pos1, _)) =>
          op ` Directive_setup_define.get
          #>> (case name of "f3" => curry op * 152263 | _ => curry op + 1)
          #>  tap (fn (nb, _) =>
                    tracing ("Executing antiquotation " ^ name ^ Position.here pos1
                             ^ " (number = " ^ Int.toString nb ^ ")"))
          #>  uncurry Directive_setup_define.put)
    \<close>
*/
#define f1
#define f2 int a = 0;
#define f3
        f1
        f2
        f1
        f3

//@ #setup \<comment> \<open>Resetting \<open>#define\<close>\<close> \<open>setup_define2 \<^here> (K I)\<close>
        f3
#define f3
        f3
\<close>

C \<comment> \<open>Dynamic token computing in \<open>#define\<close>\<close> \<open>

//@ #setup \<open>setup_define1 \<^here> (K [])\<close>
#define f int a = 0;
        f f f f

//@ #setup \<open>setup_define1 \<^here> (fn toks => toks @ toks)\<close>
#define f int b = a;
        f f

//@ #setup \<open>setup_define1 \<^here> I\<close>
#define f int a = 0;
        f f
\<close>

section \<open>Miscellaneous\<close>

C \<comment> \<open>Antiquotations acting on a parsed-subtree\<close> \<open>
# /**/ include  <a\b\\c> // backslash rendered unescaped
f(){0 +  0;} /**/  // val _ : theory => 'a => theory
# /* context */ if if elif
#include <stdio.h>
if then else ;
# /* zzz */  elif /**/
#else\
            
#define FOO  00 0 "" ((
FOO(FOO(a,b,c))
#endif\<close>

C \<comment> \<open>Header-names in directives\<close> \<open>
#define F <stdio.h>
#define G "stdio\h" // expecting an error whenever expanded
#define H "stdio_h" // can be used anywhere without errors
int f = /*F*/ "";
int g = /*G*/ "";
int h =   H   "";

#include F
\<close>

C \<comment> \<open>Parsing tokens as directives only when detecting space symbols before \<open>#\<close>\<close> \<open>/*
 */ \
    \

 //
         #  /*
*/   define /**/ \
 a
a a /*#include <>*/ // must not be considered as a directive
\<close>

C \<comment> \<open>Universal character names in identifiers and Isabelle symbols\<close> \<open>
#include <stdio.h>
int main () {
  char * _ = "\x00001";
  char *  _  = " ";
  char * ó\<^url>ò = "ó\<^url>ò";
  printf ("%s %s", ó\<^url>ò, _ );
}
\<close>



ML\<open>

signature C11_AST_LIB =
  sig
    (* some general combinators *)
    val fold_either: ('a -> 'b -> 'c) -> ('d -> 'b -> 'c) -> ('a, 'd) C_Ast.either -> 'b -> 'c
    val fold_optiona: ('a -> 'b -> 'b) -> 'a C_Ast.optiona -> 'b -> 'b


    (* conversions of enumeration types to string codes *)
    val toString_cBinaryOp: C_Ast.cBinaryOp -> string
    val toString_cIntFlag: C_Ast.cIntFlag -> string
    val toString_cIntRepr: C_Ast.cIntRepr -> string
    val toString_cUnaryOp: C_Ast.cUnaryOp -> string
    val toString_cAssignOp: C_Ast.cAssignOp -> string
    val toString_abr_string: C_Ast.abr_string -> string


    (* a generic iterator collection over the entire C11 - AST. The lexical 
       "leaves" of the AST's are parametric ('a). THe collecyot function "g" (see below)
       gets as additional parameter a string-key representing its term key
       (and sometimes local information such as enumeration type keys). *)
    (* Caveat : Assembly is currently not supported *)
    (* currently a special case since idents are not properly abstracted in the src files of the
       AST generation: *)
    val fold_ident: 'a -> (string -> 'a -> 'b -> 'c) -> C_Ast.ident -> 'b -> 'c
    (* the "Leaf" has to be delivered from the context, the internal non-parametric nodeinfo
       is currently ignored. HACK. *)

    val fold_cInteger: (string -> 'a -> 'b) -> C_Ast.cInteger -> 'a -> 'b
    val fold_cConstant: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cConstant -> 'b -> 'b
    val fold_cStringLiteral: (string -> 'a -> 'b -> 'c) -> 'a C_Ast.cStringLiteral -> 'b -> 'c

    val fold_cArraySize: 'a -> (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cArraySize -> 'b -> 'b
    val fold_cAttribute: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cAttribute -> 'b -> 'b
    val fold_cBuiltinThing: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cBuiltinThing -> 'b -> 'b
    val fold_cCompoundBlockItem: (string -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cCompoundBlockItem -> 'b -> 'b
    val fold_cDeclaration: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cDeclaration -> 'b -> 'b
    val fold_cDeclarationSpecifier: (string -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cDeclarationSpecifier -> 'b -> 'b
    val fold_cDeclarator: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cDeclarator -> 'b -> 'b
    val fold_cDerivedDeclarator: (string -> 'a -> 'b -> 'b) 
                                    -> 'a C_Ast.cDerivedDeclarator -> 'b -> 'b
    val fold_cEnumeration: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cEnumeration -> 'b -> 'b
    val fold_cExpression: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cExpression -> 'b -> 'b
    val fold_cInitializer: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cInitializer -> 'b -> 'b
    val fold_cPartDesignator: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cPartDesignator -> 'b -> 'b
    val fold_cStatement: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cStatement -> 'b -> 'b
    val fold_cTypeQualifier: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cTypeQualifier -> 'b -> 'b
    val fold_cTypeSpecifier: (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cTypeSpecifier -> 'b -> 'b
    val fold_cStructureUnion : (string -> 'a -> 'b -> 'b) -> 'a C_Ast.cStructureUnion -> 'b -> 'b 

  end


structure C11_Ast_Lib  : C11_AST_LIB   =
struct 
local open  C_Ast in



fun fold_optiona _ None st = st | fold_optiona g (Some a) st = g a st;
fun fold_either g1 _ (Left a) st    = g1 a st
   |fold_either _ g2 (Right a) st   = g2 a st

fun toString_cStructTag CStructTag0 = "CStructTag0"
   |toString_cStructTag CUnionTag0  = "CUnionTag0"

fun toString_cIntFlag FlagUnsigned0 = "FlagUnsigned0"
  | toString_cIntFlag FlagLong0     = "FlagLong0"
  | toString_cIntFlag FlagLongLong0 = "FlagLongLong0"
  | toString_cIntFlag FlagImag0     = "FlagImag0"

fun toString_cIntRepr DecRepr0      = "DecRepr0"
  | toString_cIntRepr HexRepr0      = "HexRepr0"
  | toString_cIntRepr OctalRepr0    = "OctalRepr0"


fun toString_cUnaryOp CPreIncOp0    = "CPreIncOp0"
  | toString_cUnaryOp CPreDecOp0    = "CPreDecOp0"
  | toString_cUnaryOp CPostIncOp0   = "CPostIncOp0"
  | toString_cUnaryOp CPostDecOp0   = "CPostDecOp0"
  | toString_cUnaryOp CAdrOp0       = "CAdrOp0"
  | toString_cUnaryOp CIndOp0       = "CIndOp0"
  | toString_cUnaryOp CPlusOp0      = "CPlusOp0"
  | toString_cUnaryOp CMinOp0       = "CMinOp0"
  | toString_cUnaryOp CCompOp0      = "CCompOp0"
  | toString_cUnaryOp CNegOp0       = "CNegOp0"
                                    
fun toString_cAssignOp CAssignOp0   = "CAssignOp0"
  | toString_cAssignOp CMulAssOp0   = "CMulAssOp0"
  | toString_cAssignOp CDivAssOp0   = "CDivAssOp0"
  | toString_cAssignOp CRmdAssOp0   = "CRmdAssOp0"
  | toString_cAssignOp CAddAssOp0   = "CAddAssOp0"
  | toString_cAssignOp CSubAssOp0   = "CSubAssOp0"
  | toString_cAssignOp CShlAssOp0   = "CShlAssOp0"
  | toString_cAssignOp CShrAssOp0   = "CShrAssOp0"
  | toString_cAssignOp CAndAssOp0   = "CAndAssOp0"
  | toString_cAssignOp CXorAssOp0   = "CXorAssOp0"
  | toString_cAssignOp COrAssOp0    = "COrAssOp0"

fun toString_cBinaryOp CMulOp0      = "CMulOp0"
  | toString_cBinaryOp CDivOp0      = "CDivOp0"
  | toString_cBinaryOp CRmdOp0      = "CRmdOp0"
  | toString_cBinaryOp CAddOp0      = "CAddOp0"
  | toString_cBinaryOp CSubOp0      = "CSubOp0"
  | toString_cBinaryOp CShlOp0      = "CShlOp0"
  | toString_cBinaryOp CShrOp0      = "CShrOp0"
  | toString_cBinaryOp CLeOp0       = "CLeOp0"
  | toString_cBinaryOp CGrOp0       = "CGrOp0"
  | toString_cBinaryOp CLeqOp0      = "CLeqOp0"
  | toString_cBinaryOp CGeqOp0      = "CGeqOp0"
  | toString_cBinaryOp CEqOp0       = "CEqOp0"
  | toString_cBinaryOp CNeqOp0      = "CNeqOp0"
  | toString_cBinaryOp CAndOp0      = "CAndOp0"
  | toString_cBinaryOp CXorOp0      = "CXorOp0"
  | toString_cBinaryOp COrOp0       = "COrOp0"
  | toString_cBinaryOp CLndOp0      = "CLndOp0"
  | toString_cBinaryOp CLorOp0      = "CLorOp0"

fun toString_cIntFlag FlagUnsigned0 = "FlagUnsigned0"
  | toString_cIntFlag FlagLong0     = "FlagLong0"
  | toString_cIntFlag FlagLongLong0 = "FlagLongLong0"
  | toString_cIntFlag FlagImag0     = "FlagImag0"

fun toString_cIntRepr DecRepr0      = "DecRepr0"
  | toString_cIntRepr HexRepr0      = "HexRepr0"
  | toString_cIntRepr OctalRepr0    = "OctalRepr0"


fun toString_abr_string S = case  to_String_b_a_s_e S of 
                               ST X => X
                             | STa _=> error("don't know what to do with toString_abr_string STa")
 

(* could and should be done much more: change this on demand. *)
fun fold_cInteger g' (CInteger0 (i: int, _: cIntRepr, _:cIntFlag flags)) st =  
                                                          st |> g' ("CInteger0 "^Int.toString i)
fun fold_cChar   g' (CChar0(_ : char, _:bool)) st       = st |> g' "CChar0"
  | fold_cChar   g' (CChars0(_ : char list, _:bool)) st = st |> g' "CChars0"
fun fold_cFloat  g' (CFloat0 (_: abr_string)) st        = st |> g' "CChars0"
fun fold_cString g' (CString0 (_: abr_string, _: bool)) st = st |> g' "CString0"


fun fold_cConstant g (CIntConst0 (i: cInteger, a))  st = st |> fold_cInteger (fn x=>g x a) i
                                                            |> g "CIntConst0" a  
  | fold_cConstant g (CCharConst0 (c : cChar, a))   st = st |> fold_cChar (fn x=>g x a) c
                                                            |> g "CCharConst0" a   
  | fold_cConstant g (CFloatConst0 (f : cFloat, a)) st = st |> fold_cFloat (fn x=>g x a) f
                                                            |> g "CFloatConst0" a   
  | fold_cConstant g (CStrConst0 (s : cString, a))  st = st |> fold_cString (fn x=>g x a) s
                                                            |> g "CStrConst0" a

fun fold_ident a g (Ident0(_: abr_string, _ : int, _: nodeInfo (* ignored here !!! *))) st = 
                         st |> g "Ident0" a
    (* there could be done much much more !!! *)
    (* that ident contains nodeinfo instead of 'a is an error in my view. *)

fun fold_cStringLiteral g (CStrLit0(_:cString, a)) st =  st |> g "CStrLit0" a 


fun  fold_cTypeSpecifier g (CAtomicType0 (decl : 'a cDeclaration, a)) st = 
                                  st |> fold_cDeclaration g decl |> g "CAtomicType0" a
   | fold_cTypeSpecifier g (CBoolType0 a)     st = st |> g "CBoolType0" a
   | fold_cTypeSpecifier g (CCharType0 a)     st = st |> g "CCharType0" a 
   | fold_cTypeSpecifier g (CComplexType0 a)  st = st |> g "CComplexType0" a 
   | fold_cTypeSpecifier g (CDoubleType0 a)   st = st |> g "CDoubleType0" a 
   | fold_cTypeSpecifier g (CEnumType0(e: 'a cEnumeration, a)) st = 
                                              st |> fold_cEnumeration g e
                                                 |> g "CEnumType0" a
   | fold_cTypeSpecifier g (CFloatType0 a)    st = st |> g "CFloatType0" a
   | fold_cTypeSpecifier g (CInt128Type0 a)   st = st |> g "CInt128Type0" a 
   | fold_cTypeSpecifier g (CIntType0 a)      st = st |> g "CIntType0" a 
   | fold_cTypeSpecifier g (CLongType0 a)     st = st |> g "CLongType0" a 
   | fold_cTypeSpecifier g (CSUType0 (su: 'a cStructureUnion, a))  st = 
                                              st |> fold_cStructureUnion g su
                                                 |> g "CSUType0" a
   | fold_cTypeSpecifier g (CShortType0 a)    st = st |> g "CShortType0" a 
   | fold_cTypeSpecifier g (CSignedType0 a)   st = st |> g "CSignedType0" a 
   | fold_cTypeSpecifier g (CTypeDef0 (id:ident, a)) st = 
                                              st |>  fold_ident a g id
                                                 |>  g "CTypeDef0" a 
   | fold_cTypeSpecifier g (CTypeOfExpr0 (ex: 'a cExpression, a)) st = 
                                              st |> fold_cExpression g ex 
                                                 |> g "CTypeOfExpr0" a
   | fold_cTypeSpecifier g (CTypeOfType0 (decl: 'a cDeclaration, a)) st = 
                                              st |> fold_cDeclaration g decl 
                                                 |> g "CTypeOfType0" a
   | fold_cTypeSpecifier g (CUnsigType0 a)    st = st |> g "CUnsigType0" a
   | fold_cTypeSpecifier g (CVoidType0 a)     st = st |> g "CVoidType0"  a


and  fold_cTypeQualifier g (CAtomicQual0 a) st = g "CAtomicQual0" a st
   | fold_cTypeQualifier g (CAttrQual0 (CAttr0 (id,eL:'a cExpression list, a))) st = 
                                              st |> fold_ident a g id
                                                 |> fold(fold_cExpression g) eL
                                                 |> g "CAttrQual0" a
   | fold_cTypeQualifier g (CConstQual0 a) st    = st |> g "CConstQual0" a
   | fold_cTypeQualifier g (CNonnullQual0 a) st  = st |> g "CNonnullQual0" a 
   | fold_cTypeQualifier g (CNullableQual0 a) st = st |> g "CNullableQual0" a
   | fold_cTypeQualifier g (CRestrQual0 a) st    = st |> g "CRestrQual0" a
   | fold_cTypeQualifier g (CVolatQual0 a) st    = st |> g "CVolatQual0" a 

and  fold_cStatement g (CLabel0(id:ident, s:'a cStatement, 
                                aL: 'a cAttribute list, a)) st= 
                                  st |> fold_ident a g id
                                     |> fold_cStatement g s
                                     |> fold(fold_cAttribute g) aL
                                     |> g "CLabel0" a 
   | fold_cStatement g (CCase0(ex: 'a cExpression, 
                               stmt: 'a cStatement, a)) st    = 
                                  st |> fold_cExpression g ex
                                     |> fold_cStatement g stmt
                                     |> g "CCase0" a
   | fold_cStatement g (CCases0(ex1: 'a cExpression, 
                                ex2: 'a cExpression,
                                stmt:'a cStatement, a)) st   = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2
                                     |> fold_cStatement g stmt
                                     |> g "CCases0" a
   | fold_cStatement g (CDefault0(stmt:'a cStatement, a)) st  = 
                                  st |> fold_cStatement g stmt
                                     |> g "CDefault0" a
   | fold_cStatement g (CExpr0(ex_opt:'a cExpression optiona, a)) st = 
                                  st |> fold_optiona (fold_cExpression g) ex_opt
                                     |> g "CExpr0" a
   | fold_cStatement g (CCompound0(idS : ident list, 
                                   cbiS: 'a cCompoundBlockItem list, a)) st = (* TODO *)
                                  st |> fold(fold_ident a g) idS
                                     |> fold(fold_cCompoundBlockItem g) cbiS
                                     |> g "CCompound0" a
   | fold_cStatement g (CIf0(ex1:'a cExpression,stmt: 'a cStatement, 
                        stmt_opt: 'a cStatement optiona, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> fold_optiona (fold_cStatement g) stmt_opt
                                     |> g "CIf0" a
   | fold_cStatement g (CSwitch0(ex1:'a cExpression, 
                                 stmt: 'a cStatement, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> g "CSwitch0" a
   | fold_cStatement g (CWhile0(ex1:'a cExpression, 
                                stmt: 'a cStatement, b: bool, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cStatement g stmt
                                     |> g ("CWhile0 "^((b ? (K "F")) "T")) a
   | fold_cStatement g (CFor0(ex0:('a cExpression optiona, 'a cDeclaration) either, 
                              ex1_opt: 'a cExpression optiona, 
                              ex2_opt: 'a cExpression optiona,
                              stmt: 'a cStatement, a)) st = 
                                  st |> fold_either (fold_optiona (fold_cExpression g)) 
                                                    (fold_cDeclaration g) ex0
                                     |> fold_optiona (fold_cExpression g) ex1_opt
                                     |> fold_optiona (fold_cExpression g) ex2_opt
                                     |> fold_cStatement g stmt
                                     |> g "CFor0" a
   | fold_cStatement g (CGoto0(id: ident, a)) st = 
                                  st |>  fold_ident a g id
                                     |> g "CGoto0" a
   | fold_cStatement g (CGotoPtr0(ex1:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex1 |> g "CGotoPtr0" a
   | fold_cStatement g (CCont0 a)  st = st |> g "CCont0" a
   | fold_cStatement g (CBreak0 a) st = st |> g "CBreak0" a
   | fold_cStatement g (CReturn0 (ex:'a cExpression optiona,a)) st = 
                                  st |> fold_optiona (fold_cExpression g) ex |> g "CReturn0" a
   | fold_cStatement g (CAsm0(_: 'a cAssemblyStatement, a)) st = 
                                  (* assembly ignored so far *)
                                  st |> g "CAsm0" a

and fold_cExpression g (CComma0 (eL:'a cExpression list, a)) st = 
                                 st |> fold(fold_cExpression g) eL |> g "CComma0" a
  | fold_cExpression g (CAssign0(aop:cAssignOp, 
                                 ex1:'a cExpression,
                                 ex2:'a cExpression,a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2 
                                     |> g ("CAssign0 "^toString_cAssignOp aop) a
  | fold_cExpression g (CCond0(  ex1:'a cExpression, 
                                 ex2opt: 'a cExpression optiona, (* bescheuert ! Wieso option ?*) 
                                 ex3: 'a cExpression,a)) st = 
                                  st |> fold_cExpression g ex1 
                                     |> fold_optiona (fold_cExpression g) ex2opt
                                     |> fold_cExpression g ex3 |> g "CCond0" a
  | fold_cExpression g (CBinary0(bop: cBinaryOp, ex1: 'a cExpression,ex2: 'a cExpression, a)) st =
                                  st |> fold_cExpression g ex1 
                                     |> fold_cExpression g ex2 
                                     |> g ("CBinary0 "^toString_cBinaryOp bop) a 
  | fold_cExpression g (CCast0(decl:'a cDeclaration, ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cDeclaration g decl
                                     |> g "CCast0" a
  | fold_cExpression g (CUnary0(unop:cUnaryOp, ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> g ("CUnary0 "^toString_cUnaryOp unop) a
  | fold_cExpression g (CSizeofExpr0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex    |> g "CSizeofExpr0" a
  | fold_cExpression g (CSizeofType0(decl:'a cDeclaration,a)) st = 
                                  st |> fold_cDeclaration g decl |> g "CSizeofType0" a
  | fold_cExpression g (CAlignofExpr0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex    |> g "CAlignofExpr0" a
  | fold_cExpression g (CAlignofType0(decl:'a cDeclaration, a)) st = 
                                  st |> fold_cDeclaration g decl |> g "CAlignofType0" a
  | fold_cExpression g (CComplexReal0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g "CComplexReal0" a
  | fold_cExpression g (CComplexImag0(ex:'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g "CComplexImag0" a
  | fold_cExpression g (CIndex0(ex1:'a cExpression, ex2: 'a cExpression, a)) st =
                                  st |> fold_cExpression g ex1 
                                     |> fold_cExpression g ex2 
                                     |> g "CIndex0" a
  | fold_cExpression g (CCall0(ex:'a cExpression, argS: 'a cExpression list, a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold (fold_cExpression g) argS 
                                     |> g "CCall0" a
  | fold_cExpression g (CMember0(ex:'a cExpression, id:ident, b, a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold_ident a g id
                                     |> g ("CMember0"^((b ? (K "F")) "T")) a 
  | fold_cExpression g (CVar0(id:ident,a)) st = st |> fold_ident a g id |> g "CVar0" a
  | fold_cExpression g (CConst0(cc:'a cConstant)) st  = st |> fold_cConstant g cc
  | fold_cExpression g (CCompoundLit0(decl:'a cDeclaration,
                                      eqn: ('a cPartDesignator list * 'a cInitializer) list, a)) st =
                                  st |> fold(fn(S,init) => 
                                             fn st => st |> fold(fold_cPartDesignator g) S
                                                         |> fold_cInitializer g init) eqn 
                                     |> fold_cDeclaration g decl 
                                     |> g "CCompoundLit0" a 
  | fold_cExpression g (CGenericSelection0(ex:'a cExpression, 
                                           eqn: ('a cDeclaration optiona*'a cExpression)list,a)) st =
                                  st |> fold_cExpression g ex 
                                     |> fold (fn (d,ex) => 
                                              fn st => st |> fold_optiona (fold_cDeclaration g) d  
                                                          |> fold_cExpression g ex) eqn  
                                     |> g "CGenericSelection0" a  
  | fold_cExpression g (CStatExpr0(stmt: 'a cStatement,a)) st =  
                                  st |> fold_cStatement g stmt |> g "CStatExpr0" a
  | fold_cExpression g (CLabAddrExpr0(id:ident,a)) st = 
                                  st |> fold_ident a g id |> g "CLabAddrExpr0" a 
  | fold_cExpression g (CBuiltinExpr0(X: 'a cBuiltinThing)) st = st |> fold_cBuiltinThing g X
  
and fold_cDeclaration g (CDecl0(dsS : 'a cDeclarationSpecifier list, 
                                mkS: (('a cDeclarator optiona*'a cInitializer optiona) * 'a cExpression optiona) list,
                                a)) st = 
                                  st |> fold(fold_cDeclarationSpecifier g) dsS 
                                     |> fold(fn ((d_o, init_o),ex_opt) =>
                                             fn st => st |> fold_optiona(fold_cDeclarator g) d_o
                                                         |> fold_optiona(fold_cInitializer g) init_o
                                                         |> fold_optiona(fold_cExpression g) ex_opt) mkS
                                    |> g "CDecl0" a
  | fold_cDeclaration g (CStaticAssert0(ex:'a cExpression, slit: 'a cStringLiteral, a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cStringLiteral g slit
                                     |> g "CStaticAssert0" a

and fold_cBuiltinThing g (CBuiltinVaArg0(ex:'a cExpression,decl: 'a cDeclaration,a)) st = 
                                  st |> fold_cExpression g ex 
                                     |> fold_cDeclaration g decl
                                     |> g "CBuiltinVaArg0" a 
  | fold_cBuiltinThing g (CBuiltinOffsetOf0(d: 'a cDeclaration, _: 'a cPartDesignator list, a)) st = 
                                  st |> fold_cDeclaration g d 
                                     |> g "CBuiltinOffsetOf0" a 
  | fold_cBuiltinThing g (CBuiltinTypesCompatible0 (d1: 'a cDeclaration, d2: 'a cDeclaration,a)) st= 
                                  st |> fold_cDeclaration g d1
                                     |> fold_cDeclaration g d2 
                                     |> g "CBuiltinTypesCompatible0" a 

and  fold_cInitializer g (CInitExpr0(ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g "CInitExpr0" a 
   | fold_cInitializer g (CInitList0 (mms: ('a cPartDesignator list * 'a cInitializer) list,a)) st = 
                                  st |> fold(fn (a,b) => 
                                             fn st => st|> fold(fold_cPartDesignator g) a 
                                                        |> fold_cInitializer g b) mms
                                     |> g "CInitList0" a

and  fold_cPartDesignator g (CArrDesig0(ex: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex |> g "CArrDesig0" a 
   | fold_cPartDesignator g (CMemberDesig0(id: ident, a)) st = 
                                  st |> fold_ident a g id |> g "CMemberDesig0" a 
   | fold_cPartDesignator g (CRangeDesig0(ex1: 'a cExpression, ex2: 'a cExpression, a)) st = 
                                  st |> fold_cExpression g ex1
                                     |> fold_cExpression g ex2
                                     |> g "CRangeDesig0" a 

and fold_cAttribute g (CAttr0(id: ident, exS: 'a cExpression list, a)) st =
                                  st |> fold_ident a g id
                                     |> fold(fold_cExpression g) exS 
                                     |> g "CAttr0" a 

and fold_cEnumeration g (CEnum0 (ident_opt: ident optiona,
                              exS_opt: ((ident * 'a cExpression optiona) list) optiona,
                              attrS: 'a cAttribute list, a)) st = 
                                  st |> fold_optiona(fold_ident a g) ident_opt
                                     |> fold_optiona(fold(
                                             fn (id,ex_o) =>
                                             fn st => st |> fold_ident a g id
                                                         |> fold_optiona (fold_cExpression g) ex_o)) 
                                                      exS_opt   
                                     |> fold(fold_cAttribute g) attrS
                                     |> g "CEnum0" a 



and fold_cArraySize a g (CNoArrSize0 (b: bool)) st = 
                                  st |> g ("CNoArrSize0 "^((b ? (K "F")) "T")) a 
  | fold_cArraySize a g (CArrSize0 (b:bool, ex : 'a cExpression)) st = 
                                  st |> fold_cExpression g ex 
                                     |> g ("CNoArrSize0 "^((b ? (K "F")) "T")) a 


and fold_cDerivedDeclarator g (CPtrDeclr0 (tqS: 'a cTypeQualifier list , a)) st = 
                                  st |> fold(fold_cTypeQualifier g) tqS 
                                     |> g "CPtrDeclr0" a
  | fold_cDerivedDeclarator g (CArrDeclr0 (tqS:'a cTypeQualifier list, aS: 'a cArraySize,a)) st = 
                                  st |> fold(fold_cTypeQualifier g) tqS 
                                     |> fold_cArraySize a g aS 
                                     |> g "CArrDeclr0" a
  | fold_cDerivedDeclarator g (CFunDeclr0 (decl_alt: (ident list, 
                                                      ('a cDeclaration list * bool)) either, 
                                           aS: 'a cAttribute list, a)) st = 
                                  st |> fold_either 
                                             (fold(fold_ident a g)) 
                                             (fn (declS,b) =>
                                              fn st => st |> fold (fold_cDeclaration g) declS
                                                          |> g("CFunDeclr0-decl_alt-Right-"^
                                                               ((b ? (K "F")) "T")) a) decl_alt
                                     |> fold(fold_cAttribute g) aS 
                                     |> g "CFunDeclr0" a

and fold_cDeclarationSpecifier g (CStorageSpec0(CAuto0 a)) st = 
                                  st |> g "CStorageSpec0-CAuto0" a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CRegister0 a)) st = 
                                  st |> g "CStorageSpec0-CRegister0" a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CStatic0 a)) st = 
                                  st |> g "CStorageSpec0-CStatic0" a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CExtern0 a)) st = 
                                  st |> g "CStorageSpec0-CExtern0" a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CTypedef0 a)) st = 
                                  st |> g "CStorageSpec0-CTypedef0" a
   |fold_cDeclarationSpecifier g (CStorageSpec0(CThread0 a)) st = 
                                  st |> g "CStorageSpec0-CThread0" a

   |fold_cDeclarationSpecifier g (CTypeSpec0(CVoidType0 a)) st = 
                                  st |> g "CTypeSpec0-CVoidType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CCharType0 a)) st = 
                                  st |> g "CTypeSpec0-CCharType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CShortType0 a)) st = 
                                  st |> g "CTypeSpec0-CShortType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CIntType0 a)) st = 
                                  st |> g "CTypeSpec0-CIntType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CLongType0 a)) st = 
                                  st |> g "CTypeSpec0-CLongType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CFloatType0 a)) st = 
                                  st |> g "CTypeSpec0-CFloatType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CDoubleType0 a)) st = 
                                  st |> g "CTypeSpec0-CDoubleType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CSignedType0 a)) st = 
                                  st |> g "CTypeSpec0-CSignedType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CUnsigType0 a)) st = 
                                  st |> g "CTypeSpec0-CUnsigType0" a
   |fold_cDeclarationSpecifier g (CTypeSpec0(CBoolType0 a)) st = 
                                  st |> g "CTypeSpec0-CBoolType0" a

   |fold_cDeclarationSpecifier g (CTypeQual0(x: 'a cTypeQualifier)) st =
                                  st |> fold_cTypeQualifier g x

   |fold_cDeclarationSpecifier g (CFunSpec0(CInlineQual0 a)) st = 
                                  st |> g "CFunSpec0-CInlineQual0" a 
   |fold_cDeclarationSpecifier g (CFunSpec0(CNoreturnQual0 a)) st = 
                                  st |> g "CFunSpec0-CNoreturnQual0" a 
 
   |fold_cDeclarationSpecifier g (CAlignSpec0(CAlignAsType0(decl,a))) st = 
                                  st |> fold_cDeclaration g decl
                                     |> g "CAlignSpec0-CAlignAsType0" a
   |fold_cDeclarationSpecifier g (CAlignSpec0(CAlignAsExpr0(ex,a))) st = 
                                  st |> fold_cExpression g ex
                                     |> g "CAlignSpec0-CAlignAsType0" a

and fold_cDeclarator g (CDeclr0(id_opt: ident optiona,
                                declS: 'a cDerivedDeclarator list,
                                sl_opt: 'a cStringLiteral optiona,
                                attrS: 'a cAttribute list, a)) st = 
                                  st |> fold_optiona(fold_ident a g) id_opt
                                     |> fold (fold_cDerivedDeclarator g) declS
                                     |> fold_optiona(fold_cStringLiteral g) sl_opt
                                     |> fold(fold_cAttribute g) attrS
                                     |> g"CDeclr0" a

and fold_cFunctionDef g (CFunDef0(dspecS: 'a cDeclarationSpecifier list,
                                  dclr: 'a cDeclarator,
                                  declsS: 'a cDeclaration list,
                                  stmt: 'a cStatement, a)) st = 
                                    st |> fold(fold_cDeclarationSpecifier g) dspecS
                                       |> fold_cDeclarator g dclr
                                       |> fold(fold_cDeclaration g) declsS
                                       |> fold_cStatement g stmt
                                       |> g "CFunDef0" a

and fold_cCompoundBlockItem g (CBlockStmt0 (stmt: 'a cStatement)) st = 
                                    st |> fold_cStatement g stmt 
  | fold_cCompoundBlockItem g (CBlockDecl0 (decl : 'a cDeclaration)) st = 
                                    st |> fold_cDeclaration g decl 
  | fold_cCompoundBlockItem g (CNestedFunDef0(fdef : 'a cFunctionDef)) st = 
                                    st |> fold_cFunctionDef g fdef

and fold_cStructureUnion g (CStruct0(  ct : cStructTag, id_a: ident optiona, 
                                       declS_opt : ('a cDeclaration list) optiona,
                                       aS: 'a cAttribute list, a)) st = 
                                    st |> fold_optiona (fold_ident a g) id_a
                                       |> fold_optiona (fold(fold_cDeclaration g)) declS_opt
                                       |> fold(fold_cAttribute g) aS
                                       |> g ("CStruct0"^toString_cStructTag ct) a  


end

end (*struct *)
\<close>

ML\<open>

\<close>



ML\<open>open C11_Ast_Lib  open C_Ast 


\<close>

end
